trigger QuoteTrigger on Quote (after insert) {
	switch on Trigger.operationType {
        when AFTER_INSERT {
            QuoteTriggerHandler.afterInsertFillQuoteVersion(Trigger.newMap);
        }
    }
}