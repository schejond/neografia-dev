/**
 * Trigger on Order
 *
 * @author  Ondrej Schejbal
 * @date    2022-02-17
 */
trigger OrderTrigger on Order (before update) {
    switch on Trigger.operationType {
        when BEFORE_UPDATE {
            OrderTriggerHandler.modifyStatusBeforeCancellation(Trigger.newMap, Trigger.oldMap);
        }
    }
}