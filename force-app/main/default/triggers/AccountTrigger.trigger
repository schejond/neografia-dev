/**
 * Trigger on Account
 *
 * @author  Ondrej Schejbal - Aspectworks
 * @date    2022-02-17
 */
trigger AccountTrigger on Account (before insert, before update) {
    switch on Trigger.operationType {
        when BEFORE_INSERT {
            AccountTriggerHandler.beforeInsertModifyAccountingInformationBasedOnRegion(Trigger.new);
        }
        when BEFORE_UPDATE {
            AccountTriggerHandler.beforeUpdateModifyAccountingInformationIfRegionChanged(Trigger.newMap, Trigger.oldMap);
        }
    }
}