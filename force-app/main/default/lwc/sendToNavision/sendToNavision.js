import { api, LightningElement, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { CloseActionScreenEvent } from 'lightning/actions';

// custom labels
import error_message from '@salesforce/label/c.error_message';
import error_title from '@salesforce/label/c.error_title';
import send_button_label from '@salesforce/label/c.send_button_label';
import success_message from '@salesforce/label/c.success_message';
import success_title from '@salesforce/label/c.success_title';
import title_1st_part from '@salesforce/label/c.title_1st_part';
import title_2nd_part from '@salesforce/label/c.title_2nd_part';

import sendAccountToNavision from '@salesforce/apex/SendToNavisionController.sendAccountToNavision';
import sendContactToNavision from '@salesforce/apex/SendToNavisionController.sendContactToNavision';

import ACC_CUSTOMER_CODE from '@salesforce/schema/Account.Customer_Code__c';
import ACC_NAME from '@salesforce/schema/Account.Name';

import CON_NAME from '@salesforce/schema/Contact.Name';
import CON_EMAIL from '@salesforce/schema/Contact.Email';

export default class SendToNavision extends LightningElement {
    @api recordId;
    @api objectApiName;
    @track showSpinner = false;
    fieldsToDisplay = [];
    hasRendered = false;
    title = '';
    label = {
        error_message, error_title, send_button_label, success_message,
        success_title, title_1st_part, title_2nd_part
    };

    renderedCallback() {
        if (!this.hasRendered && this.objectApiName && this.recordId) {
            this.title = this.label.title_1st_part + ' ' + this.objectApiName + ' ' + this.label.title_2nd_part;
    
            switch(this.objectApiName) {
                case 'Account':
                    this.fieldsToDisplay = [ACC_CUSTOMER_CODE, ACC_NAME];
                    break;
                case 'Contact':
                    this.fieldsToDisplay = [CON_NAME, CON_EMAIL];
                    break;
                default:
                    console.error(`Undefined scenario! Component used on unexpected sObject ${this.objectApiName}`);
                    return;
            }

            this.hasRendered = true;
        }
    }

    sendToNAV() {
        console.log(`Sending ${this.objectApiName} data to Navision!`);

        let sendRecordToNavision = undefined;
        switch(this.objectApiName) {
            case 'Account':
                sendRecordToNavision = sendAccountToNavision;
                break;
            case 'Contact':
                sendRecordToNavision = sendContactToNavision;
                break;
            default:
                console.error(`Undefined scenario! Component used on unexpected sObject ${this.objectApiName}`);
                return;
        }
        this.showSpinner = true;
        sendRecordToNavision({
            recordId: this.recordId
        }).then(response => {
            // todo - logika na zpracovani odpovedi            
            // if (nejaka podminka) {
            //     return getArticlesByTopicIdMap({topicIds: 'neco'});
            // }
            // return Promise.resolve([]);
        // }).then(articlesByTopicId => {
            console.log('Zpracovavam odpoved');

            this.dispatchEvent(new ShowToastEvent({
                title: this.label.success_title,
                message: this.label.success_message,
                variant: 'success',
                mode: 'dismissible'
            }));
        }).catch(err => {
            console.error(`An error occured when sending ${this.objectApiName} data to Navision!`, err);
            this.dispatchEvent(new ShowToastEvent({
                title: this.label.error_title,
                message: this.label.error_message,
                variant: 'error',
                mode: 'sticky'
            }));
        }).finally(() => {
            this.showSpinner = false;
            this.dispatchEvent(new CloseActionScreenEvent());
        });
    }
}