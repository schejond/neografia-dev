import { api, LightningElement } from 'lwc';

export default class TestHeadlessQuickAction extends LightningElement {
    @api invoke() {
        console.log("Hi, I'm an action.");
    }
}