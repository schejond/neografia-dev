import { api, LightningElement } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

// custom labels
import error_message from '@salesforce/label/c.error_message';
import error_title from '@salesforce/label/c.error_title';
import success_message from '@salesforce/label/c.success_message';
import success_title from '@salesforce/label/c.success_title';

import sendRecordToNavision from '@salesforce/apex/SendToNavisionController.sendRecordToNavision';

export default class SendToNavisionHeadless extends LightningElement {
    @api recordId;
    @api objectApiName;
    isExecuting = false;
    label = {
        error_message, error_title, success_message, success_title
    };

    @api async invoke() {
        if (this.isExecuting) {
            return;
        }

        this.isExecuting = true;
        console.log(`Sending ${this.objectApiName} data to Navision!`);

        sendRecordToNavision({
            recordId: this.recordId,
            sObjectName: this.objectApiName
        }).then(res => {
            console.log('Call to Navision suceeded.\nResponse from Navision:\n', res);
            if (res?.responseCode == 200)
            {
                this.dispatchEvent(new ShowToastEvent({
                    title: this.label.success_title,
                    message: this.label.success_message,
                    variant: 'success',
                    mode: 'dismissible'
                }));
            }
            else
            {
                console.error(`Received ${res?.responseCode} response from Navision.`, res?.response);
                this.dispatchEvent(new ShowToastEvent({
                    title: this.label.error_title,
                    message: this.label.error_message,
                    variant: 'error',
                    mode: 'sticky'
                }));
            }
        //     if (cond) {
        //         return apexMethod({topicIds: 'neco'});
        //     }
        //     return Promise.resolve([]);
        // }).then(articlesByTopicId => {
        }).catch(err => {
            console.error(`An error occured when sending ${this.objectApiName} data to Navision.`, err?.body?.message);
            this.dispatchEvent(new ShowToastEvent({
                title: this.label.error_title,
                message: this.label.error_message,
                variant: 'error',
                mode: 'sticky'
            }));
        }).finally( () => {this.isExecuting = false;});
    }
}