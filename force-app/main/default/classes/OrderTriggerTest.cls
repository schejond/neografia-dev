/**
 * @author  Ondrej Schejbal
 * @date    2022-02-17
 */
@IsTest(SeeAllData=false)
public with sharing class OrderTriggerTest {
    @IsTest
    private static void testModifyStatusBeforeCancellation() {
        Account acc = new Account(Name='Test Account', Customer_Code__c = '666');
        insert acc;

        List<Order> orders = new List<Order>();
        for (Integer i = 0 ; i < 200 ; i++) {
            orders.add(new Order(Status='Check date', AccountId=acc.Id, EffectiveDate=Date.parse('1/2/2015')));
        }
        insert orders;

        for (Order order : orders) {
            order.Status = 'Cancelled';
        }

        Test.startTest();
        update orders;
        Test.stopTest();

        List<Order> updatedOrders = [SELECT Id, Status, Status_Before_Cancellation__c FROM Order];
        for (Order order : updatedOrders) {
            System.assertEquals('Cancelled', order.Status);
            System.assertEquals('Check date', order.Status_Before_Cancellation__c);
        }
    }
}