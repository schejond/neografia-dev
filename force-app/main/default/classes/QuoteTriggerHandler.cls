/**
 * Handler class for Quote Trigger
 *
 * @author  Ondrej Schejbal
 * @date    2022-04-13
 */
public class QuoteTriggerHandler {
	// fills in the quote version number
	// decimal part of the number if the quote is clonned
	// integer number if its not a clone
	public static void afterInsertFillQuoteVersion(Map<Id, Quote> triggerNewMap) {
		// query the related opps
		Map<Quote, Id> quoteToRelatedOppId = new Map<Quote, Id>();
		List<Quote> clonnedQuotes = new List<Quote>();
		for (Quote q : triggerNewMap.values()) {
			if (!q.isClone()) {
				quoteToRelatedOppId.put(q, q.OpportunityId);
			} else {
				clonnedQuotes.add(q);
			}
		}

		List<Quote> quotesForUpdate = setQuoteVersionForNewQuote(quoteToRelatedOppId);
		quotesForUpdate.addAll(setQuoteVersionBasedOnClonnedQuote(clonnedQuotes));
		update quotesForUpdate;
    }

	// fills in the version number for new quotes that are not clones
	@TestVisible
	private static List<Quote> setQuoteVersionForNewQuote(Map<Quote, Id> quoteToRelatedOppId) {
		final List<Quote> relatedQuotes = [
			SELECT Id, OpportunityId, Quote_Version__c
			FROM Quote
			WHERE OpportunityId IN :quoteToRelatedOppId.values()
		];

		// for each opportunity id prepare the next version number available
		Map<Id, Integer> oppIdToNextVersionNumber = new Map<Id, Integer>();
		for (Quote q : relatedQuotes) {
			if (!oppIdToNextVersionNumber.containsKey(q.OpportunityId)) {
				oppIdToNextVersionNumber.put(q.OpportunityId, 1);
			}
			if (q.Quote_Version__c != null && q.Quote_Version__c == Integer.valueOf(q.Quote_Version__c)) {
				oppIdToNextVersionNumber.put(q.OpportunityId, oppIdToNextVersionNumber.get(q.OpportunityId) + 1);
			}
		}

		// prepare quotes with filled version number
		List<Quote> quotesWithFilledQuoteVersion = new List<Quote>();
		for (Quote q : quoteToRelatedOppId.keySet()) {
			final Integer nextVersionNumber = oppIdToNextVersionNumber.get(q.OpportunityId);
			quotesWithFilledQuoteVersion.add(
				new Quote(Id = q.Id, Quote_Version__c = nextVersionNumber)
			);
			oppIdToNextVersionNumber.put(q.OpportunityId, nextVersionNumber + 1);
		}

		return quotesWithFilledQuoteVersion;
	}

	// fills in the version number for clonned quotes
	@TestVisible
	private static List<Quote> setQuoteVersionBasedOnClonnedQuote(List<Quote> clonnedQuotes) {
		Set<Id> relatedOppIds = new Set<Id>();
		for (Quote q : clonnedQuotes) {
			relatedOppIds.add(q.OpportunityId);
		}

		// soql all quotes related to the same opps
		Map<Id, Quote> quotesRelatedThroughOpp = new Map<Id, Quote>(
			[
				SELECT Id, OpportunityId, Quote_Version__c
				FROM Quote
				WHERE OpportunityId IN :relatedOppIds
			]
		);

		// for each opportunity define the next clone numbers
		Map<Id, Map<Integer, Integer>> oppIdToIntegerBaseToCloneCnt = new Map<Id, Map<Integer, Integer>>();
		for (Quote q : quotesRelatedThroughOpp.values()) {
			if (!oppIdToIntegerBaseToCloneCnt.containsKey(q.OpportunityId)) {
				oppIdToIntegerBaseToCloneCnt.put(q.OpportunityId, new Map<Integer, Integer>());
			}
			if (q.Quote_Version__c != null) {
				final Integer roundedVersionNumber = Integer.valueOf(q.Quote_Version__c);
				if (!oppIdToIntegerBaseToCloneCnt.get(q.OpportunityId).containsKey(roundedVersionNumber)) {
					oppIdToIntegerBaseToCloneCnt.get(q.OpportunityId).put(roundedVersionNumber, 1);
				}
				// if it's a clone, increment the clone count
				if (q.Quote_Version__c != roundedVersionNumber) {
					oppIdToIntegerBaseToCloneCnt.get(q.OpportunityId).put(
						roundedVersionNumber,
						oppIdToIntegerBaseToCloneCnt.get(q.OpportunityId).get(roundedVersionNumber) + 1
					);
				}
			}
		}

		// prepare quotes with filled version number
		List<Quote> quotesWithFilledQuoteVersion = new List<Quote>();
		Map<Id, List<Id>> sourceQuoteIdToListOfNewQuoteIds = new Map<Id, List<Id>>();
		for (Quote q : clonnedQuotes) {
			final Integer sourceQuoteVersionNumberRounded = Integer.valueOf(quotesRelatedThroughOpp.get(q.getCloneSourceId()).Quote_Version__c);
			if (quotesRelatedThroughOpp.get(q.getCloneSourceId()).Quote_Version__c == null) {
				q.addError(System.Label.version_missing);
				continue;
			}

			// used for clonning related QuoteLineItems
			if (!sourceQuoteIdToListOfNewQuoteIds.containsKey(q.getCloneSourceId())) {
				sourceQuoteIdToListOfNewQuoteIds.put(q.getCloneSourceId(), new List<Id>());
			}
			sourceQuoteIdToListOfNewQuoteIds.get(q.getCloneSourceId()).add(q.Id);

			final Integer fractionPart = oppIdToIntegerBaseToCloneCnt.get(q.OpportunityId).get(sourceQuoteVersionNumberRounded);
			if (fractionPart > 99) {
				q.addError(System.Label.maximum_clones_reached);
				continue;
			}
			String decimalNumberStr = sourceQuoteVersionNumberRounded + '.';
			if (fractionPart < 10) {
				decimalNumberStr += '0';
			}
			decimalNumberStr += fractionPart;

			Quote quoteWithFilledQuoteVersion = new Quote(Id = q.Id, Quote_Version__c = Decimal.valueOf(decimalNumberStr));
			
			// NEOI-152 - add parent quote id to the lookup field
			quoteWithFilledQuoteVersion.Parent_Quote__c = q.getCloneSourceId();

			quotesWithFilledQuoteVersion.add(quoteWithFilledQuoteVersion);

			oppIdToIntegerBaseToCloneCnt.get(q.OpportunityId).put(sourceQuoteVersionNumberRounded, fractionPart + 1);
		}

		// clone related QuoteLineItems
		cloneQuoteLineItems(sourceQuoteIdToListOfNewQuoteIds);

		return quotesWithFilledQuoteVersion; 
	}

	// clone related QuoteLineItems for new clonned Quotes
	@TestVisible
	private static void cloneQuoteLineItems(Map<Id, List<Id>> sourceQuoteIdToListOfNewQuoteIds) {
		if (sourceQuoteIdToListOfNewQuoteIds.isEmpty()) {
			return;
		}

		final Set<Id> sourceQuoteIds = sourceQuoteIdToListOfNewQuoteIds.keySet();
		final List<String> qliFields = new List<String>(Schema.SObjectType.QuoteLineItem.fields.getMap().keySet());
		final List<QuoteLineItem> relatedQuoteLineItems = Database.query(
			'SELECT ' + String.join(qliFields, ', ') + ' FROM QuoteLineItem WHERE QuoteId IN :sourceQuoteIds'
		);

		List<QuoteLineItem> qlisToInsert = new List<QuoteLineItem>();
		for (QuoteLineItem qli : relatedQuoteLineItems) {
			for (Id newQuoteId : sourceQuoteIdToListOfNewQuoteIds.get(qli.QuoteId)) {
				QuoteLineItem clonnedQli = qli.clone(false, true, false, false);
				clonnedQli.QuoteId = newQuoteId;
				qlisToInsert.add(clonnedQli);
			}
		}

		insert qlisToInsert;
	}
}