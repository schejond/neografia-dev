/**
 * Created by Ondrej Schejbal on 07.03.2022
 */
public with sharing class SendToNavisionController
{
    private static SOAP_ImportAccount.ImportCustomer_Port proxy_ImportCustomer;
    private static SOAP_ImportContact.ImportContact_Port proxy_ImportContact;

    static
    {
        proxy_ImportCustomer = new SOAP_ImportAccount.ImportCustomer_Port();
        proxy_ImportContact = new SOAP_ImportContact.ImportContact_Port();
    }

    /**
     *  Calls relevant ws based on the provided SObjectName.
     *
     *  @param  recordId - Id of Account record which needs to be sent to Navision
     *  @param  SObjectName - String name of the SObject
     *  @return Response from the WS
     */
    @AuraEnabled
    public static ServerResponse sendRecordToNavision(final Id recordId, final String sObjectName)
    {
        switch on sObjectName
        {
            when 'Account'
            {
                return sendAccountToNavision(recordId);
            }
            when 'Contact'
            {
                return sendContactToNavision(recordId);
            }
            when else
            {
                throw new AuraHandledException('Undefined scenario! Component used on unexpected sObject ' + sObjectName);
            }
        }
    }

    /**
     *  Sends Account with relevant data to Navision WS.
     *
     *  @param  recordId - Id of Account record which needs to be sent to Navision
     *  @return Response from the WS
     */
    @TestVisible
    private static ServerResponse sendAccountToNavision(final Id recordId)
    {
        try
        {
            final Account acc = (Account)getRecordById(recordId, 'Account');
            // prepare customer record to send
            SOAP_Customer.customer customer = new SOAP_Customer.customer();
            customer.CustomerCode = acc.Customer_Code__c;
            customer.Name = acc.Name;
            customer.Country = acc.ShippingCountry;
            customer.Region = acc.Region__c;
            customer.Street = acc.ShippingStreet;
            customer.Street2 = acc.Street_2__c;
            customer.City = acc.ShippingCity;
            customer.PostCode = acc.ShippingPostalCode;
            customer.ParentAccount = '';
            if (String.isNotBlank(acc.ParentId))
            {
                final Account parentAcc = [SELECT Name FROM Account WHERE Id = :acc.ParentId LIMIT 1];
                customer.ParentAccount = parentAcc.Name;
            }
            customer.SellerCode = acc.Seller_Code__c;
            customer.Gen_Bus_PostingGroup = acc.General_Picklist__c;
            customer.VATBus_PostingGroup = acc.VAT__c;
            customer.CustomerPostingGroup = acc.Customer_Posting__c;
            customer.Language = acc.Language__c;
            customer.Currency_x = acc.Currency__c;
            customer.PaymentConditionCode = acc.Payment_Condition_Code__c;
            customer.CreditLimit = String.valueOf(acc.Credit_Limit__c);
            customer.ICO = acc.ICO__c;
            customer.DIC = acc.DIC__c;

            // prepare communication objects
            SOAP_Customer.XMLRoot xmlRoot = new SOAP_Customer.XMLRoot();
            xmlRoot.customer = new List<SOAP_Customer.customer> { customer };

            ServerResponse response = proxy_ImportCustomer.ImportCustomer_Http(xmlRoot);
            return response;
        }
        catch (Exception e)
        {
            throw new AuraHandledException(e.getMessage());
        }
    }

    /**
     *  Sends Contact with relevant data to Navision WS.
     *
     *  @param  recordId - Id of Contact record which needs to be sent to Navision
     *  @return Response from the WS
     */
    @TestVisible
    public static ServerResponse sendContactToNavision(final Id recordId) {
        try
        {
            final Contact con = (Contact)getRecordById(recordId, 'Contact');
            // prepare contact record to send
            SOAP_Contact.contact contact = new SOAP_Contact.contact();
            contact.ContactNumber = con.Contact_Number__c;
            contact.Customer = con.AccountId;
            contact.ContactName = con.Name;
            contact.LastName = con.LastName;
            contact.FirstName = con.FirstName;
            contact.JobTitle = con.Title;
            contact.Email = con.Email;
            contact.Phone = con.Phone;
            contact.Mobile = con.MobilePhone;
            contact.Primarny = con.PrimaryNAV__c ? 1 : 0;

            // prepare communication objects
            SOAP_Contact.XMLRoot xmlRoot = new SOAP_Contact.XMLRoot();
            xmlRoot.contact = new List<SOAP_Contact.contact> { contact };

            ServerResponse response = proxy_ImportContact.ImportContact_Http(xmlRoot);
            return response;
        }
        catch (Exception e)
        {
            throw new AuraHandledException(e.getMessage());
        }
    }

    /**
     *  Queries record with its all fields based on provided SObject name and recordId
     *
     *  @param  recordId
     *  @param  SObjectName
     *  @return Found record
     */
    private static SObject getRecordById(final Id recordId, final String sObjectName)
    {
        final String fields = String.join(
            new List<String>(Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap().keySet()),
            ', '
        );
        final String query = 'SELECT ' + fields +
                             ' FROM ' + sObjectName +
                             ' WHERE Id = :recordId' +
                             ' LIMIT 1';
        final List<sObject> foundRecords = Database.query(query);

        return foundRecords.get(0);
    }

    public class ServerResponse
    {
        @AuraEnabled
        public Integer responseCode {get; set;}
        @AuraEnabled
        public String response {get; set;}
        @AuraEnabled
        public String faultCode {get; set;}

        public ServerResponse(Integer response_code, String res)
        {
            responseCode = response_code;
            response = res;
            faultCode = '';
        }

        public ServerResponse(Integer response_code, String res, String fault_code)
        {
            responseCode = response_code;
            response = res;
            faultCode = fault_code;
        }
    }
}