/**
 * @author  Ondrej Schejbal
 * @date    2022-02-17
 */
@IsTest
private with sharing class AccountTriggerTest {
    @IsTest
    private static void testBeforeInsert_ModifyAccountingInformationBasedOnRegion() {
        List<Account> accList = new List<Account>();
        for (Integer i = 0 ; i < 200 ; i++) {
            accList.add(new Account(Name = 'Test Account ' + i, Region__c = 'SK', Customer_Code__c = '1' + i));
        }

        Test.startTest();
        insert accList;
        Test.stopTest();

        List<Account> accListAfter = [SELECT Id, Customer_Posting__c FROM Account];
        System.assertEquals(accList.size(), accListAfter.size());
        for (Account acc : accListAfter) {
            System.assertEquals('O_TUZ', acc.Customer_Posting__c);
        }
    }

    @IsTest
    private static void testBeforeUpdate_ModifyAccountingInformationBasedOnRegion() {
        List<Account> accList = new List<Account>();
        for (Integer i = 0 ; i < 200 ; i++) {
            accList.add(new Account(Name = 'Test Account ' + i, Region__c = 'non_EU', Customer_Code__c = '1' + i));
        }
        insert accList;
        List<Account> accListAfterInsert = [SELECT Id, Region__c, VAT__c FROM Account];
        accListAfterInsert.get(0).Region__c = null;
        for (Integer i = 1 ; i < accListAfterInsert.size() ; i++) {
            accListAfterInsert.get(i).Region__c = null;
            accListAfterInsert.get(i).VAT__c = 'BEZ';
        }

        Test.startTest();
        update accListAfterInsert;
        Test.stopTest();

        List<Account> accListAfterUpdate = [SELECT Id, Region__c, VAT__c, Customer_Posting__c FROM Account];
        System.assertEquals(accList.size(), accListAfterUpdate.size());
        for (Integer i = 0 ; i < accListAfterUpdate.size() ; i++) {
            System.assertEquals(null, accListAfterUpdate.get(i).Region__c);
            System.assertEquals('O_TUZ', accListAfterUpdate.get(i).Customer_Posting__c);
            if (i == 0) {
                System.assertEquals('TUZ', accListAfterUpdate.get(i).VAT__c);
            } else {
                System.assertEquals('BEZ', accListAfterUpdate.get(i).VAT__c);
            }
        }
    }
}