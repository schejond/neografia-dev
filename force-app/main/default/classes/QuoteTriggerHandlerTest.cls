/**
 * @author  Ondrej Schejbal
 * @date    2022-04-13
 */
@IsTest(SeeAllData=false)
private class QuoteTriggerHandlerTest {
	@IsTest
    private static void testSetQuoteVersionForNewQuote() {
        Account acc = new Account(Name = 'Test acc', Customer_Code__c = '123', DIC__c = '123', ICO__c = '123');
        insert acc;

        Opportunity opp = new Opportunity(
            Name = 'Test opp',
            AccountId = acc.Id,
            CloseDate = Date.today().addDays(1),
            StageName = 'Potential'
        );
        insert opp;

        List<Quote> quotes = new List<Quote>();
        for (Integer i = 0 ; i < 200 ; i++) {
            quotes.add(
                new Quote(Name = 'Quote ' + i, OpportunityId = opp.Id)
            );
        }
        insert quotes;

        Map<Quote, Id> quoteToRelatedOppId = new Map<Quote,Id>();
        for (Quote q : quotes) {
            quoteToRelatedOppId.put(q, q.OpportunityId);
        }

        Test.startTest();
        final List<Quote> result = QuoteTriggerHandler.setQuoteVersionForNewQuote(quoteToRelatedOppId);
        Test.stopTest();

        for (Integer i = 0 ; i < result.size() ; i++) {
            System.assertEquals(i + 201, result.get(i).Quote_Version__c);
        }
    }

    @IsTest
    private static void testSetQuoteVersionBasedOnClonnedQuote() {
        Account acc = new Account(Name = 'Test acc', Customer_Code__c = '123', DIC__c = '123', ICO__c = '123');
        insert acc;

        Opportunity opp = new Opportunity(
            Name = 'Test opp',
            AccountId = acc.Id,
            CloseDate = Date.today().addDays(1),
            StageName = 'Potential'
        );
        insert opp;

        Quote initialQuote = new Quote(Name = 'Quote 1', OpportunityId = opp.Id);
        insert initialQuote;

        List<Quote> clonnedQuotes = new List<Quote>();
        // maximum of 99 clones is allowed
        for (Integer i = 0 ; i < 99 ; i++) {
            clonnedQuotes.add(
                initialQuote.clone()
            );
        }

        Test.startTest();
        final List<Quote> result = QuoteTriggerHandler.setQuoteVersionBasedOnClonnedQuote(clonnedQuotes);
        Test.stopTest();

        for (Integer i = 0 ; i < result.size() ; i++) {
            String decimalNumberStr = '1.';
            if (i + 1 < 10) {
                decimalNumberStr += '0';
            }
			decimalNumberStr += (i + 1);
            System.assertEquals(Decimal.valueOf(decimalNumberStr), result.get(i).Quote_Version__c);
        }
    }
}