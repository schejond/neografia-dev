public class Utility {
    // splits long string into multiple debug logs
    public static void debugLongString(String text, String prefix)
    {
        Integer i = 0;
        String contentInfo = '';
        
        if (!String.isBlank(prefix))
        {
            contentInfo = ' for ' + prefix + ':';
        }

        if (text.length() <= 350)
        {
            System.debug(prefix + ': ' + text);
            return;
        }

		System.debug('[debugLongString] START' + contentInfo);
		do
        {
			if (i < text.length())
            {
				System.debug('' + text.substring(i, Math.min(i + 350, text.length())));
			}
		    i = i + 350;
		} while (i < text.length());
		System.debug('[debugLongString] END');
    }
}