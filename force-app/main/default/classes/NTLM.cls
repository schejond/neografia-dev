/**------------------------------------------------------------
 * Author:        Ondrej Schejbal
 * Company:       Seyfor
 * Description:   Classes to handle NTLM auth
 *------------------------------------------------------------*/
public with sharing class NTLM {

    /**
     * The flags that are supported.
     * (NegotiateUnicode, NegotiateOEM, RequestTarget, NegotiateNTLMKey, NegotiateAlwaysSign, NegotiateNTLM2Key)
     */
    private static final integer NEGOTIATE_FLAGS = 557575;

    /**
     * The size of the header in type 3 messages
     */
    private static final integer MESSAGE_TYPE_3_HEADER_SIZE = 64;

    public static String mode = 'NTLM';

    /**
     * This class wraps calls to the Http.send method defined by the system and
     * adds support for NTLM authentication
     *
     * @author Nate Wallace
     */
    public class HttpClient {

        /**
         * Supports the getUser/setUser methods
         */
        @TestVisible
        private string mUser;

        /**
         * Supports the getPassword/setPassword methods
         */
        @TestVisible
        private string mPassword;

        /**
         * Supports the setIsPreAuthenticate/getIsPreAuthenticate methods
         */
        private boolean mIsPreAuthenticate;

        /**
         * Used to send http messages
         */
        private Http mHttp;

        /**
         * Used for NTLM authentication
         */
        private NTLMAuth mNtlm;

        /**
         * @Author:          Ondrej Schejbal
         * @Company:         Seyfor
         * @Description:     Constructor, loads login credentials from custom settings
         *------------------------------------------------------------*/
        public HttpClient() {
            Neografia_SOAP_Auth_Settings__c settings = Neografia_SOAP_Auth_Settings__c.getOrgDefaults();
            setUser(settings.Domain__c + '/' + settings.Username__c);
            setPassword(settings.Password__c);
            setIsPreAuthenticate(settings.IsPreAuthenticate__c);
            mHttp = new Http();
            mNtlm = new NTLMAuth();
        }

        /**
         * Constructor
         *
         * @param user User
         * @param password Password
         */
        public HttpClient(string user, string password) {
            this(user, password, false);
        }

        /**
         * Constructor
         *
         * @param user User
         * @param password Password
         * @param isPreAuthenticate IsPreAuthenticate
         */
        public HttpClient(string user, string password, boolean isPreAuthenticate) {
            setUser(user);
            setPassword(password);
            setIsPreAuthenticate(isPreAuthenticate);
            mHttp = new Http();
            mNtlm = new NTLMAuth();
        }

        /**
         * Set the User part of authentication credentials
         * This should be the full domain and user name for NTLM authentication.  i.e. UP\DUX07
         *
         * @param value The User part of authentication credentials
         */
        public void setUser(string value) {
            mUser = value;
        }

        /**
         * Get the User part of authentication credentials.  If not set this defaults to null
         *
         * @return The User part of authentication credentials
         */
        public string getUser() {
            return mUser;
        }

        /**
         * Set the Password part of authentication credentials
         *
         * @param value The Password part of authentication credentials
         */
        public void setPassword(string value) {
            mPassword = value;
        }

        /**
         * Get the Password part of authentication credentials
         *
         * @return The Password part of authentication credentials. If not set this defaults to null
         */
        public string getPassword() {
            return mPassword;
        }

        /**
         * When set to true this object will send authentication information with the first message instead of waiting for a challenge from the server
         * When set to false this object will not send authentication information until the server challenges us
         *
         * @param value The IsPreAuthenticate value to set
         */
        public void setIsPreAuthenticate(boolean value) {
            mIsPreAuthenticate = value;
        }

        /**
         * When this returns true this object will send authentication information with the first message instead of waiting for a challenge from the server
         * When this returns false this object will not send authentication information until the server challenges us
         * If not set this defaults to false
         *
         * @returns value The IsPreAuthenticate value that is set
         */
        public boolean getIsPreAuthenticate() {
            return mIsPreAuthenticate;
        }

        /**
         * Sends an HttpRequest and returns the response
         *
         * @param request The request to send.  Note that only the Endpoint, Method, Compressed, and Body values are used
         * @param headers The headers to include in the request
         * @return The response from the server
         */
        public HttpResponse send(HttpRequest request, Map<string, string> headers) {
            if (request.getHeader('Content-Length') == null)
            {
                addContentLengthHeader(request);
            }
            integer authStep = 0;
            HttpRequest req = Clone(request, headers, false);
            if (getIsPreAuthenticate())
            {
                req.setHeader('Authorization', mode + ' ' + mNtlm.getType1Message());
                req.setBody('');
                authStep = 1;
            }

            HttpResponse resp = mHttp.send(req);
            // process a 401 response
            while (resp.getStatusCode() == 401)
            {
                // System.debug('Got 401 response. AuthStep = ' + authStep);
                if (authStep > 1) {
                    System.debug('Repeatably received 401 response more than once - give up on retrying');
                    return resp;
                }
                resp = handle401Response(resp, request, headers, authStep);
                authStep++;
            }

            // System.debug('Response code: ' + resp.getStatusCode());
            return resp;
        }

        private void addContentLengthHeader(HttpRequest request){
            Integer length = 0;
            if (request.getBodyAsBlob() != null) {
                length = request.getBodyAsBlob().size();
            }
            request.setHeader('Content-Length', '' + length);
            // System.debug('Content-Length set to ' + length);
        }

        /**
         * Sends an HttpRequest and returns the response
         *
         * @param request The request to send.  Note that only the Endpoint, Method, Compressed, and Body values are used.
         * @return The response from the server.
         */
        public HttpResponse send(HttpRequest request) {
            return send(request, null);
        }

        /**
         * Handle the 401 response and perform NTLM authentication
         *
         * @param response The response that came back from the server with a 401
         * @param request The original request
         * @param headers The headers for the original request
         * @param authStep The current step we are on in the authorization sequence
         * @return A new response from the server after sending out an appropriate NTLM message in response to the 401
         */
        private HttpResponse handle401Response(HttpResponse response, HttpRequest request, Map<string, string> headers, integer authStep) {
            if (authStep == 0) {    // send initial NTLM type 1 message
                HttpRequest req = Clone(request, headers, false);
                req.setHeader('Authorization', mode + ' ' + mNtlm.getType1Message());
                req.setBody('');
                HttpResponse resp = mHttp.send(req);
                return resp;
            } else if (authStep == 1) { // send NTLM type 3 message in response to a type 2 message received from the server
                string ntlmMessage = ParseHttpNtlmHeader(response);
                if (ntlmMessage == null || ntlmMessage == '')
                    return response; // Server didn't send message after we sent it our type 1 message so we give up
                HttpRequest req = Clone(request, headers, true);
                req.setHeader('Authorization', mode + ' ' + mNtlm.getType3Message(ntlmMessage, getUser(), getPassword()));
                HttpResponse resp = mHttp.send(req);
                return resp;
            } else { // we have given up after the second step
                return response;
            }
        }

        /**
         * Clones the given request
         *
         * @param request The request to clone
         * @param headers The headers to include in the request
         * @return The new object that is a clone of the request parameter
         */
        private HttpRequest Clone(HttpRequest request, Map<string, string> headers, Boolean copyBody) {
            HttpRequest req = new HttpRequest();
            req.setEndpoint(request.getEndpoint());
            req.setMethod(request.getMethod());
            req.setCompressed(request.getCompressed());
            if (copyBody) {
                if (request.getBodyAsBlob() != null) {
                    req.setBodyAsBlob(request.getBodyAsBlob());
                }
                if (request.getBodyDocument() != null) {
                    req.setBodyDocument(request.getBodyDocument());
                }
                addContentLengthHeader(req);
            }
            for (String header : NTLM.headers){
                if (request.getHeader(header) != null){
                    req.setHeader(header, request.getHeader(header));
                }
            }
			req.setTimeout(60000);
            if (headers != null)
                for (string key : headers.keySet())
                    req.setHeader(key, headers.get(key));
            return req;
        }

        /**
         * Parses the given response headers for the NTLM message
         *
         * @param response The response whose headers will be parsed.
         * @return An empty string if the header found indicates NTLM authentication should be initiated. i.e. When the header WWW-Authenticate: Negotiate,NTLM is found.
         *        An NTLM message when the header found includes an NTLM message.  i.e. WWW-Authenticate: NTLM TlRMTVNTUAABAAAAB4IIAAAAAAAAAAAAAAAAAAAAAAA= will return TlRMTVNTUAABAAAAB4IIAAAAAAAAAAAAAAAAAAAAAAA=.
         *        A null value will be returned if there is no NTLM header or the authentication specified in the header doesn't support NTLM authentication.
         */
        public string ParseHttpNtlmHeader(HttpResponse response) {
            if (response == null)
                throw new RuntimeException('response is null.');
            string header = response.getHeader('WWW-Authenticate');
            if (header == null)
                return null;
            if (header.startsWith(mode + ' '))
                return header.removeStart(mode + ' ');
            string[] headerValues = header.split(',', -1);
            for (string headerValue : headerValues) {
                if (headerValue == mode)
                    return '';
            }
            return null;
        }
    }

    /*
     * This class is used to perform NTLM authentication and is based on documentation found on the following site:
     * http://davenport.sourceforge.net/ntlm.html as well as code from the open source Chromium project which can be found here:
     * https://code.google.com/p/chromium/codesearch#chromium/src/net/http/http_auth_handler_ntlm_portable.cc&q=ntlmssp&sq=package:chromium&l=137
     *
     * The NTLM authentication process goes as follows:
     * 1.) send type 1 message to the server.
     * 2.) recieve type 2 message from the server in response to type 1 message.
     * 3.) send type 3 message to the server in response to type 2 message.
     */
    public class NTLMAuth {

        /**
         * Get type 1 message to send to the server.
         *
         * @return A type 1 message that can be sent to the server to begin authentication.
         */
        public string getType1Message() {
            integer[] result = new integer[]{
                    78, 84, 76, 77, 83, 83, 80, 0,                      // 0:  NTLMSSP
                    1, 0, 0, 0
            };                                         // 8:  Type marker
            result.addAll(BitConverter.getBytesForUInt32(NEGOTIATE_FLAGS));     // 12: Flags
            WriteSecurityBuffer(result, null, null);                        // 16: Domain security buffer (empty)
            WriteSecurityBuffer(result, null, null);                        // 24: Workstation security buffer (empty)
            return BitConverter.encodeBase64String(result);
        }

        /**
         * Get type 3 message to send to the server.
         *
         * @param type2Message The response from the server after sending the type 1 message generated by a call to getType1Message.
         * @param user The domain and username used for authentication which should formatted like the example: UP\DUX07
         * @param password The password used for authentication.
         */
        public string getType3Message(string type2Message, string user, string password) {
            // separate domain and user
            integer slashIndex = user.indexOfAny('/\\');
            if (slashIndex == -1) {
                System.debug('User parameter doesn\'t include a domain value');
                throw new RuntimeException('user parameter doesn\'t include a domain value.');
            }
            string userDomain = user.subString(0, slashIndex);
            string userName = user.subString(slashIndex + 1, user.length());
            //
            // parse and validate type 2 message
            //
            integer[] type2Bytes = BitConverter.decodeBase64String(type2Message);
            if (type2Bytes.size() < 32)
                throw new RuntimeException('type2Message is less than minimum 32 bytes required');
            if (type2Bytes [0] != 78 ||
                    type2Bytes [1] != 84 ||
                    type2Bytes [2] != 76 ||
                    type2Bytes [3] != 77 ||
                    type2Bytes [4] != 83 ||
                    type2Bytes [5] != 83 ||
                    type2Bytes [6] != 80 ||
                    type2Bytes [7] != 0)
                throw new RuntimeException('type2Message doesn\'t have a valid NTLMSSP header');
            if (type2Bytes[8] != 2)
                throw new RuntimeException('type2Message isn\'t a type 2 message');
            integer flags = BitConverter.toUInt32(type2Bytes, 20);
            boolean isUnicode = (flags & 1) == 1;
            integer[] challenge = new integer[8];
            for (integer i = 24; i < 32; i++)
                challenge[i - 24] = type2Bytes[i];
            //
            // generate responses
            //
            integer[] lmResponse = null;
            integer[] ntlmResponse = null;
            MD4Hash md4 = new MD4Hash();
            integer[] ntlmHash = md4.digest(BitConverter.EncodeUTF16String(password));
            if ((flags & 524288) > 0) { // NegotiateNTLM2Key
                long maxLong = (long) Math.pow(2, 64);
                long random = Math.roundToLong(Math.random() * maxLong);
                integer[] nonce = BitConverter.getBytesForUInt64(random);
                lmResponse = new integer[0];
                lmResponse.addAll(nonce);
                for (integer i = 0; i < 16; i++)
                    lmResponse.add(0);
                integer[] key = new integer[0];
                key.addAll(challenge);
                key.addAll(nonce);
                blob digest = Crypto.generateDigest('MD5', BitConverter.getBlob(key));
                ntlmResponse = createNTLMResponse(ntlmHash, BitConverter.getBytes(digest));
            } else { // NegotiateNTLMKey
                lmResponse = new integer[24];
                for (integer i = 0; i < 24; i++)
                    lmResponse[i] = 0;
                ntlmResponse = createNTLMResponse(ntlmHash, challenge);
            }
            //
            // populate the message
            //
            integer[] header = new integer[]{
                    78, 84, 76, 77, 83, 83, 80, 0,       // 0: NTLMSSP
                    3, 0, 0, 0
            };                // 8: Type marker
            integer[] body = new integer[0];
            // 12: LM response
            WriteSecurityBuffer(header, body, lmResponse);
            // 20: NTLM response
            WriteSecurityBuffer(header, body, ntlmResponse);
            // 28: Domain name
            WriteSecurityBuffer(header, body, isUnicode ? BitConverter.encodeUTF16String(userDomain) : BitConverter.encodeUTF8String(userDomain));
            // 36: User name
            WriteSecurityBuffer(header, body, isUnicode ? BitConverter.encodeUTF16String(userName) : BitConverter.encodeUTF8String(userName));
            // 44: Workstation (host)
            WriteSecurityBuffer(header, body, isUnicode ? BitConverter.encodeUTF16String('SalesForce') : BitConverter.encodeUTF8String('SalesForce'));
            // 52: Session key (not used)
            WriteSecurityBuffer(header, body, null);
            // 60: Negotiated flags
            header.addAll(BitConverter.getBytesForUInt32(NEGOTIATE_FLAGS & flags));
            integer[] message = new integer[0];
            message.addAll(header);
            message.addAll(body);
            return BitConverter.encodeBase64String(message);
        }

        /**
         * Writes the content out to the body and marks the location in the header
         *
         * @param header The header data that the security buffer pointer will be appended to
         * @param body The body data that the content will be appended to
         * @param content The data that will be written out to the body.  The position will be recorded in the header
                          If this value is null an empty security buffer pointer will be written to the header and the body
                        won't be modified.
         */
        private void WriteSecurityBuffer(integer[] header, integer[] body, integer[] content) {
            if (content == null || content.size() == 0) {
                header.addAll(BitConverter.getBytesForUInt16(0));
                header.addAll(BitConverter.getBytesForUInt16(0));
                header.addAll(BitConverter.getBytesForUInt32(0));
            } else {
                integer pos = MESSAGE_TYPE_3_HEADER_SIZE + body.size();
                body.addAll(content);
                header.addAll(BitConverter.getBytesForUInt16(content.size()));
                header.addAll(BitConverter.getBytesForUInt16(content.size()));
                header.addAll(BitConverter.getBytesForUInt32(pos));
            }
        }

        /**
         * Create NTLM response
         *
         * @param hash The hash to create an NTLM response with
         * @param challenge The challenge from the server to create an NTLM response with
         * @return The NTLM response created from the hash and challenge inputs
         */
        private integer[] createNTLMResponse(integer[] hash, integer[] challenge) {
            integer[] ntlmHash = new integer[0];
            ntlmHash.addAll(hash);
            for (integer i = 0; i < 5; i++)
                ntlmHash.add(0);
            integer[] lowKey = createDESKey(ntlmHash, 0);
            integer[] midKey = createDESKey(ntlmHash, 7);
            integer[] highKey = createDESKey(ntlmHash, 14);
            DESCrypt des = new DESCrypt();
            integer[] lowResponse = des.encrypt(lowKey, challenge);
            integer[] midResponse = des.encrypt(midKey, challenge);
            integer[] highResponse = des.encrypt(highKey, challenge);
            integer[] result = new integer[24];
            for (integer i = 0; i < 8; i++) {
                result[i] = lowResponse[i];
                result[i + 8] = midResponse[i];
                result[i + 16] = highResponse[i];
            }
            return result;
        }

        /**
         * Creates a DES encryption key from the given key material
         *
         * @param bytes The bytes to create a key with.
         * @param offset The offset to start reading data from in the bytes parameter
         * @return A DES encryption key.
         */
        public integer[] createDESKey(integer[] bytes, integer offset) {
            integer[] keyBytes = new integer[7];
            for (integer i = offset; i < offset + 7; i++)
                keyBytes[i - offset] = bytes[i];
            integer[] material = new integer[8];
            material[0] = keyBytes[0];
            material[1] = (((keyBytes[0] << 7) & 255) | (keyBytes[1] & 255) >>> 1);
            material[2] = (((keyBytes[1] << 6) & 255) | (keyBytes[2] & 255) >>> 2);
            material[3] = (((keyBytes[2] << 5) & 255) | (keyBytes[3] & 255) >>> 3);
            material[4] = (((keyBytes[3] << 4) & 255) | (keyBytes[4] & 255) >>> 4);
            material[5] = (((keyBytes[4] << 3) & 255) | (keyBytes[5] & 255) >>> 5);
            material[6] = (((keyBytes[5] << 2) & 255) | (keyBytes[6] & 255) >>> 6);
            material[7] = (((keyBytes[6] << 1) & 255));
            applyOddParity(material);
            return material;
        }

        /**
         * Applies odd parity to the given bytes
         *
         * @param bytes The bytes that odd parity will be applied to
         */
        private void applyOddParity(integer[] bytes) {
            for (integer i = 0; i < bytes.size(); i++) {
                integer b = bytes[i];
                boolean needsParity = (((((((((b >>> 7) ^ (b >>> 6)) ^ (b >>> 5)) ^ (b >>> 4)) ^ (b >>> 3)) ^ (b >>> 2)) ^ (b >>> 1))) & 1) == 0;
                if (needsParity){
                    bytes[i] |= 1;
                } else {
                    bytes[i] &= 254;
                }
            }
        }
    }

    public static BitConverter BitConverter = new BitConverter();
    /*
     * Methods that support bit manipulation.
     *
     * Since the apex language lacks a byte primitive, bytes are represented as integers within this class.  All integer values in this class
     * are treated as bytes unless otherwise noted.  You should never pass in an integer that is larger than 256 when a byte is expected.  There
     * are no checks for this condition but unpredictable results may occur if you do.
     *
     * All methods use little endian encoding unless otherwise noted.
     *
     * @author Nate Wallace
     */
    public class BitConverter {

        /**
         * Table of base 64 values in order
         */
        private string[] mBase64Table;

        /**
         * Mapping of base 64 values to their 6 bit equivalent
         */
        private Map<string, integer> mBase64To6BitMap;

        /**
         * Mapping of hex digit to base 10 digit
         */
        private Map<string, integer> mHexMap;

        /**
         * Initialize static members
         */
        public BitConverter() {
            mBase64Table = new string[]{
                    'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                    'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
                    '0','1','2','3','4','5','6','7','8','9','+','/'};
            mBase64To6BitMap = new Map<string, integer>();
            for (integer i = 0; i < mBase64Table.size(); i++)
                mBase64To6BitMap.put(mBase64Table[i], i);
            mHexMap = new Map<string, integer> {
                    '0' => 0,
                    '1' => 1,
                    '2' => 2,
                    '3' => 3,
                    '4' => 4,
                    '5' => 5,
                    '6' => 6,
                    '7' => 7,
                    '8' => 8,
                    '9' => 9,
                    'A' => 10,
                    'B' => 11,
                    'C' => 12,
                    'D' => 13,
                    'E' => 14,
                    'F' => 15,
                    'a' => 10,
                    'b' => 11,
                    'c' => 12,
                    'd' => 13,
                    'e' => 14,
                    'f' => 15
            };
        }

        /**
         * Convert the base 64 encoded string into an array of bytes
         *
         * @param text The base 64 encoded string to decode
         * @return The decoded base 64 string as an array of bytes
         */
        public integer[] decodeBase64String(String text) {
            if (text == null)
                throw new RuntimeException('text is null');
            if (Math.mod(text.length(), 4) != 0)
                throw new RuntimeException('text is not a valid base 64 encoded string (not a multiple of 4): ' + text);

            integer[] result = new integer[]{};

            for (integer i= 0; i < text.length(); i += 4)
            {
                string firstChar = text.substring(i, i + 1);
                string secondChar = text.substring(i + 1, i + 2);
                string thirdChar = text.substring(i + 2, i + 3);
                string fourthChar = text.substring(i + 3, i + 4);

                integer firstBit = mBase64To6BitMap.get(firstChar);
                if (firstBit == null)
                    throw new RuntimeException('Invalid base 64 character: ' + firstChar);

                integer secondBit = mBase64To6BitMap.get(secondChar);
                if (secondBit == null)
                    throw new RuntimeException('Invalid base 64 character: ' + secondBit);

                integer thirdBit = null;
                if (thirdChar != '=') {
                    thirdBit = mBase64To6BitMap.get(thirdChar);
                    if (thirdBit == null)
                        throw new RuntimeException('Invalid base 64 character: ' + thirdChar);
                }

                integer fourthBit = null;
                if (fourthChar != '=') {
                    fourthBit = mBase64To6BitMap.get(fourthChar);
                    if (fourthBit == null)
                        throw new RuntimeException('Invalid base 64 character: ' + fourthChar);
                }

                result.add((firstBit << 2) | (secondBit >> 4));
                if (thirdBit != null)
                    result.add(((secondBit & 15) << 4) | (thirdBit >> 2));
                if (thirdBit != null && fourthBit != null)
                    result.add(((thirdBit & 3) << 6) | fourthBit);
            }

            return result;
        }

        /**
         * Convert the given bytes into a base 64 encoded string
         *
         * @param bytes The bytes to convert into a base 64 encoded string
         * @return The base 64 encoded string that represents the bytes passed in
         */
        public string encodeBase64String(integer[] bytes) {

            if (bytes == null)
                throw new RuntimeException('bytes is null');

            string[] result = new string[]{};
            integer bitsLength = bytes.size();

            for (integer i = 0; i < bitsLength; i += 3)
            {
                integer firstBit = bytes[i];
                integer secondBit = (i + 1 < bitsLength) ? bytes[i + 1] : null;
                integer thirdBit = (i + 2 < bitsLength) ? bytes[i + 2] : null;

                if (firstBit == null)
                    throw new RuntimeException('bytes contains a null bit.');

                result.add(mBase64Table[(firstBit >> 2) & 63]);

                if (secondBit == null) {
                    if (i + 1 < bitsLength)
                        throw new RuntimeException('bytes contains a null bit.');

                    result.add(mBase64Table[(firstBit & 3) << 4]);
                    result.add('==');
                    break;
                }
                else if (thirdBit == null) {
                    if (i + 2 < bitsLength)
                        throw new RuntimeException('bytes contains a null bit.');

                    result.add(mBase64Table[((firstBit & 3) << 4) | ((secondBit >> 4) & 15)]);
                    result.add(mBase64Table[(secondBit & 15) << 2]);
                    result.add('=');
                    break;
                }
                else {
                    result.add(mBase64Table[((firstBit & 3) << 4) | ((secondBit >> 4) & 15)]);
                    result.add(mBase64Table[((secondBit & 15) << 2) | ((thirdBit >> 6) & 3)]);
                    result.add(mBase64Table[thirdBit & 63]);
                }
            }

            return String.join(result, '');
        }

        /**
         * Convert a string that is a hex representation of a number into an integer value
         *
         * @param hex The base 16 value to turn into a base 10 value
         * @return The base 10 value of the given base 16 value
         */
        public integer encodeIntegerFromHex(string hex) {
            if (hex == null || hex.length() == 0)
                throw new RuntimeException('hex is null or empty.');

            string reversed = hex.reverse();

            integer result = 0;
            for (integer i = 0; i < hex.length(); i++) {
                string s = reversed.subString(i, i + 1);
                integer num = mHexMap.get(s);
                if (num == null)
                    throw new RuntimeException('Invalid hex character: ' + s);
                result += (integer)(Math.pow(16, i) * num);
            }

            return result;
        }

        /**
         * Convert a string into a UTF-16 encoded byte array
         *
         * @param text The text to encode as a UTF-16 byte array
         * @return The UTF-16 encoded byte array.
         */
        public integer[] encodeUTF16String(string text) {
            integer[] result = new integer[]{};

            for (integer i = 0; i < text.length(); i++) {
                string hex = EncodingUtil.convertToHex(Blob.valueOf(text.subString(i, i + 1)));
                result.add(EncodeIntegerFromHex(hex));
                result.add(0);
            }

            return result;
        }

        /**
         * Convert a string into a UTF-8 encoded byte array
         *
         * @param text The text to encode as a UTF-8 byte array
         * @return The UTF-8 encoded byte array.
         */
        public integer[] encodeUTF8String(string text) {
            integer[] result = new integer[]{};

            for (integer i = 0; i < text.length(); i++) {
                string hex = EncodingUtil.convertToHex(Blob.valueOf(text.subString(i, i + 1)));
                result.add(EncodeIntegerFromHex(hex));
            }

            return result;
        }

        /**
         * Get complement of the given integer
         *
         * @param value The integer to get the complement for
         * @return The complement of the given integer value
         */
        public integer getIntegerComplement(integer value) {
            return (value * -1) - 1;
        }

        /**
         * Get the unsigned 32-bit integer value from the given bytes starting at the offset
         *
         * @param bytes The bytes to read the integer data from
         * @param offset The offset to start reading the integer data from
         * @return The integer read in from the given bytes
         */
        public integer toUInt32(integer[] bytes, integer offset) {
            integer result  = bytes[offset];
            result         |= bytes[offset + 1] << 8;
            result         |= bytes[offset + 2] << 16;
            result         |= bytes[offset + 3] << 24;

            return result;
        }

        /**
         * Get the unsigned 16-bit integer value from the given bytes starting at the offset
         *
         * @param offset The offset to start reading the integer data from
         * @return The integer read in from the given bytes
         */
        public integer toUInt16(integer[] bytes, integer offset) {
            integer result  = bytes[offset];
            result         |= bytes[offset + 1] << 8;

            return result;
        }

        /**
         * Get the bytes for the given blob value
         *
         * @param value The blob to convert into a byte array
         * @return The byte array converted from the given blob
         */
        public integer[] getBytes(blob value) {
            string hex = EncodingUtil.convertToHex(value);
            integer[] result = new integer[0];
            for (integer i = 0; i < hex.length(); i += 2)
                result.add(EncodeIntegerFromHex(hex.subString(i, i + 2)));

            return result;
        }

        /**
         * Get the bytes that make up the given long value
         *
         * @param value The value to convert into a byte array
         * @return The byte array converted from the given value
         */
        public integer[] getBytesForUInt64(long value) {
            integer[] result = new integer[8];
            result[7] = (integer)(value >>> 56);
            result[6] = (integer)(value >>> 48) & 255;
            result[5] = (integer)(value >>> 40) & 255;
            result[4] = (integer)(value >>> 32) & 255;
            result[3] = (integer)(value >>> 24) & 255;
            result[2] = (integer)(value >>> 16) & 255;
            result[1] = (integer)(value >>> 8)  & 255;
            result[0] = (integer)(value)        & 255;

            return result;
        }

        /**
         * Get the bytes that make up the given integer value
         *
         * @param value The value to convert into a byte array
         * @return The byte array converted from the given value
         */
        public integer[] getBytesForUInt32(integer value) {
            integer[] result = new integer[4];
            result[3] = (integer)(value >>> 24) & 255;
            result[2] = (integer)(value >>> 16) & 255;
            result[1] = (integer)(value >>> 8)  & 255;
            result[0] = (integer)(value)        & 255;

            return result;
        }

        /**
         * Get the bytes that make up the given integer value
         *
         * @param value The value to convert into a byte array
         * @return The byte array converted from the given value
         */
        public integer[] getBytesForUInt16(integer value) {
            integer[] result = new integer[2];
            result[1] = (integer)(value >>> 8)  & 255;
            result[0] = (integer)(value)        & 255;

            return result;
        }

        /**
         * Convert the given bytes into a blob
         *
         * @param bytes The bytes to convert into a blob
         * @return The blob converted from the given bytes
         */
        public blob getBlob(integer[] bytes) {
            string base64 = encodeBase64String(bytes);
            return EncodingUtil.base64Decode(base64);
        }
    }

    /*
     * Generic runtime exception
     */
    public class RuntimeException extends Exception { }

    private static List<String> headers = new List<String>{'Access-Control-Allow-Credentials', 'Access-Control-Allow-Headers',
            'Access-Control-Allow-Methods', 'Access-Control-Allow-Origin', 'Access-Control-Expose-Headers', 'Access-Control-Max-Age',
            'Accept-Ranges', 'Age', 'Allow', 'Alternate-Protocol', 'Cache-Control', 'Client-Date', 'Client-Peer', 'Client-Response-Num',
            'Connection', 'Content-Disposition', 'Content-Encoding', 'Content-Language', 'Content-Length', 'Content-Location',
            'Content-MD5', 'Content-Range', 'Content-Security-Policy, X-Content-Security-Policy, X-WebKit-CSP', 'Content-Security-Policy-Report-Only',
            'Content-Type', 'Date', 'ETag', 'Expires', 'HTTP', 'Keep-Alive', 'Last-Modified', 'Link', 'Location', 'P3P',
            'Pragma', 'Proxy-Authenticate', 'Proxy-Connection', 'Refresh', 'Retry-After', 'Server', 'Set-Cookie', 'Status',
            'Strict-Transport-Security', 'Timing-Allow-Origin', 'Trailer', 'Transfer-Encoding', 'Upgrade', 'Vary', 'Via',
            'Warning', 'WWW-Authenticate', 'X-Aspnet-Version', 'X-Content-Type-Options', 'X-Frame-Options', 'X-Permitted-Cross-Domain-Policies',
            'X-Pingback', 'X-Powered-By', 'X-Robots-Tag', 'X-UA-Compatible', 'X-XSS-Protection', 'Authorization', 'SOAPAction'};
}