/**------------------------------------------------------------
 * Author:        Ondrej Schejbal
 * Company:       Seyfor
 * Description:   Class to create MOCK for HTTP SOAP calls for class NavisionDataSync
 * Methods:       SayHello, TriggerError
 * Date:          2023-04-27
 *------------------------------------------------------------*/
@IsTest
global class Proxy_Mock implements HttpCalloutMock
{
	Boolean throwException;
	String response;
	Integer responseCode;

	public Proxy_Mock(String s, Integer resCode, Boolean throwEx)
    {		
		throwException = throwEx;
		response = s;
		responseCode = resCode;
	}

	public HttpResponse respond(HttpRequest req)
    {
		if (throwException)
        {
			CalloutException e = new CalloutException();
			e.setMessage('Validation Error(s) occurred during Foo Callout.');
			throw e;
		}

		HttpResponse resp = new HttpResponse();
		resp.setBody(response);
		resp.setStatusCode(responseCode);
		return resp;
	}
}