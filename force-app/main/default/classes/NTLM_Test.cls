/**------------------------------------------------------------
 * Author:        Ondrej Schejbal
 * Company:       Seyfor
 * Description:   Tests for NTLM class
 * Date:          2023-04-27
 *------------------------------------------------------------*/

@IsTest
private class NTLM_Test
{
    @IsTest
    private static void test_NTLMProcess()
    {
        Test.setMock(HttpCalloutMock.class, new NTLM_TestMock());
 
        NTLM.HttpClient http = new NTLM.HttpClient('domain/user', 'pswd');
        HttpRequest request = new HttpRequest();
        request.setBody('any body');
        request.setMethod('POST');
        request.setEndpoint('https://any.ws');
        http.send(request);
    }
 
    @IsTest
    private static void test_credentialsLoading()
    {
        Neografia_SOAP_Auth_Settings__c settings = Neografia_SOAP_Auth_Settings__c.getOrgDefaults();
        settings.Username__c= 'user';
        settings.Password__c = 'pswd';
        settings.Domain__c = 'domain';
        settings.IsPreAuthenticate__c = false;
        upsert settings;
 
        NTLM.HttpClient http = new NTLM.HttpClient();
 
        System.assertEquals('domain/user', http.getUser());
        System.assertEquals('pswd', http.getPassword());
        System.assertEquals(false, http.getIsPreAuthenticate());
    }
 
    @isTest
    static void test_tokenParsing(){
        String message = 'TlRMTVNTUAACAAAAEAAQADgAAAA1goriluCDYHcYI/sAAAAAAAAAAFQAVABIAAAABQLODgAAAA9TAFAASQBSAEkAVAAxAEIAAgAQAFMAUABJAFIASQBUADEAQgABABAAUwBQAEkAUgBJAFQAMQBCAAQAEABzAHAAaQByAGkAdAAxAGIAAwAQAHMAcABpAHIAaQB0ADEAYgAAAAAA';
        String decoded = new NTLM.NTLMAuth().getType3Message(message, 'domain\\user', 'pass');
    }
}