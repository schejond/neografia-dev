/**------------------------------------------------------------
 * Author:        Ondrej Schejbal
 * Company:       Seyfor
 * Description:   Tests for Utility class
 * Date:          2023-04-27
 *------------------------------------------------------------*/

@IsTest
private class UtilityTest
{
    @IsTest
    private static void test_debugLongString()
    {
        String longString = '66';
        Utility.debugLongString(longString, '');

        for (Integer i = 0 ; i < 400 ; i++)
        {
            longString += '6';
        }
        
        Utility.debugLongString(longString, 'prefix');
    }
}