/**
 * Handler class for Order Trigger
 *
 * @author  Ondrej Schejbal
 * @date    2022-02-17
 */
public class OrderTriggerHandler {
    // if the Status of Order changed to 'Cancelled', then save the last Status to the custom field
    public static void modifyStatusBeforeCancellation(Map<Id, Order> triggerNewMap, final Map<Id, Order> triggerOldMap) {
        for (Order order : triggerNewMap.values()) {
            if (order.Status != triggerOldMap.get(order.Id).Status && order.Status == 'Cancelled') {
                order.Status_Before_Cancellation__c = triggerOldMap.get(order.Id).Status;
            }
        }
    }
}