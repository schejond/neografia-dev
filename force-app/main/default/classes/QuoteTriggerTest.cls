/**
 * @author  Ondrej Schejbal
 * @date    2022-04-17
 */
@IsTest(SeeAllData=false)
private class QuoteTriggerTest {
    @IsTest
    private static void testInsertNewQuote() {
        Account acc = new Account(Name = 'Test acc', Customer_Code__c = '123', DIC__c = '123', ICO__c = '123');
        insert acc;

        Opportunity opp = new Opportunity(
            Name = 'Test opp',
            AccountId = acc.Id,
            CloseDate = Date.today().addDays(1),
            StageName = 'Potential'
        );
        insert opp;

        List<Quote> quotes = new List<Quote>();
        for (Integer i = 0 ; i < 200 ; i++) {
            quotes.add(
                new Quote(Name = 'Quote ' + i, OpportunityId = opp.Id)
            );
        }

        Map<Quote, Id> quoteToRelatedOppId = new Map<Quote,Id>();
        for (Quote q : quotes) {
            quoteToRelatedOppId.put(q, q.OpportunityId);
        }

        Test.startTest();
        insert quotes;
        Test.stopTest();

        final List<Quote> result = [SELECT Id, Quote_Version__c, Name, Parent_Quote__c FROM Quote ORDER BY Quote_Version__c ASC];

        for (Integer i = 0 ; i < result.size() ; i++) {
            System.assertEquals(i + 1, result.get(i).Quote_Version__c);
            System.assertEquals('Quote ' + i, result.get(i).Name);
            System.assertEquals(null, result.get(i).Parent_Quote__c);
        }
    }

    @IsTest
    private static void testInsertClonnedQuote() {
        Account acc = new Account(Name = 'Test acc', Customer_Code__c = '123', DIC__c = '123', ICO__c = '123');
        insert acc;

        Opportunity opp = new Opportunity(
            Name = 'Test opp',
            AccountId = acc.Id,
            CloseDate = Date.today().addDays(1),
            StageName = 'Potential'
        );
        insert opp;

        Quote initialQuote = new Quote(Name = 'Quote 1', OpportunityId = opp.Id);
        insert initialQuote;
        Quote firstClone = initialQuote.clone();
        insert firstClone;

        List<Quote> clonnedQuotes = new List<Quote>();
        // maximum of 99 clones is allowed
        for (Integer i = 0 ; i < 98 ; i++) {
            clonnedQuotes.add(
                initialQuote.clone()
            );
        }

        Test.startTest();
        insert clonnedQuotes;
        Test.stopTest();

        final List<Quote> result = [
            SELECT Quote_Version__c, Parent_Quote__c FROM Quote
            WHERE Id != :initialQuote.Id AND Id != :firstClone.Id
            ORDER BY Quote_Version__c ASC
        ];

        for (Integer i = 0 ; i < result.size() ; i++) {
            String decimalNumberStr = '1.';
            if (i + 2 < 10) {
                decimalNumberStr += '0';
            }
			decimalNumberStr += (i + 2);
            System.assertEquals(Decimal.valueOf(decimalNumberStr), result.get(i).Quote_Version__c);
            System.assertEquals(initialQuote.Id, result.get(i).Parent_Quote__c);
        }
    }

    
    @IsTest
    private static void testCloningOfQuoteLineItems() {
        Account acc = new Account(Name = 'Test acc', Customer_Code__c = '123', DIC__c = '123', ICO__c = '123');
        insert acc;

        Opportunity opp = new Opportunity(
            Name = 'Test opp',
            AccountId = acc.Id,
            CloseDate = Date.today().addDays(1),
            StageName = 'Potential'
        );
        insert opp;

        Quote quote = new Quote(
            Name = 'Quote 1', OpportunityId = opp.Id, Pricebook2Id = Test.getStandardPricebookId()
        );
        insert quote;

        Product2 prod = new Product2(Name = 'Test product');
        insert prod;

        PricebookEntry pe = new PricebookEntry(
            UnitPrice = 1.0, Pricebook2Id = Test.getStandardPricebookId(),
            Product2Id = prod.Id, IsActive = true
        );
        insert pe;

        QuoteLineItem qli = new QuoteLineItem(
            QuoteId = quote.Id, PricebookEntryId = pe.Id,
            Quantity = 6, UnitPrice = 1, Product2Id = prod.Id, Paper_Code__c = 'a'
        );
        insert qli;

        List<Quote> clonnedQuotes = new List<Quote>();
        for (Integer i = 0 ; i < 50 ; i++) {
            clonnedQuotes.add(
                quote.clone()
            );
        }

        Test.startTest();
        insert clonnedQuotes;
        Test.stopTest();

        final List<QuoteLineItem> result = [SELECT Id FROM QuoteLineItem];
        System.assertEquals(clonnedQuotes.size() + 1, result.size());
    }
}