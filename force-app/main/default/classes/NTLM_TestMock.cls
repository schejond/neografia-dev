/**------------------------------------------------------------
 * Author:        Ondrej Schejbal
 * Company:       Seyfor
 * Description:   Mock for NTLM class
 * Date:          2023-04-27
 *------------------------------------------------------------*/

@IsTest
public class NTLM_TestMock implements HttpCalloutMock
{
    private static Integer stepCounter = 0;

    public HttpResponse respond(HttpRequest httpRequest)
    {
        HttpResponse response = new HttpResponse();
        if (stepCounter < 2)
        {
            response.setBody('any response');
            response.setStatusCode(401);
            response.setHeader('WWW-Authenticate', 'NTLM TlRMTVNTUAACAAAAEAAQADgAAAA1goriluCDYHcYI/sAAAAAAAAAAFQAVABIAAAABQLODgAAAA9TAFAASQBSAEkAVAAxAEIAAgAQAFMAUABJAFIASQBUADEAQgABABAAUwBQAEkAUgBJAFQAMQBCAAQAEABzAHAAaQByAGkAdAAxAGIAAwAQAHMAcABpAHIAaQB0ADEAYgAAAAAA');
            stepCounter++;
        }
        else
        {
            response.setBody('any response');
            response.setStatusCode(200);
        }
        return response;
    }
}