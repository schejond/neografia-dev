/**
 * Handler class for Account Trigger
 *
 * @author  Ondrej Schejbal
 * @date    2022-02-17
 */
public class AccountTriggerHandler {
	public static void beforeInsertModifyAccountingInformationBasedOnRegion(List<Account> triggerNew) {
		for (Account acc : triggerNew) {
			if (acc.General_Picklist__c == null) {
				changeGeneralPostingGroupBasedOnRegion(acc.Region__c, acc);
			}

			if (acc.Customer_Posting__c == null) {
				changeCustomerPostingGroupBasedOnRegion(acc.Region__c, acc);
			}

			if (acc.VAT__c == null) {
				changeVATPostingGroupBasedOnRegion(acc.Region__c, acc);
			}
		}
	}
	
	public static void beforeUpdateModifyAccountingInformationIfRegionChanged(Map<Id, Account> triggerNewMap, final Map<Id, Account> triggerOldMap) {
		for (Account acc : triggerNewMap.values()) {
			if (acc.Region__c != triggerOldMap.get(acc.Id).Region__c) {
				// if the old and new values don't match it means that the agent made some specific changes
				// and we don't apply automatic field change
				if (acc.General_Picklist__c == triggerOldMap.get(acc.Id).General_Picklist__c) {
					changeGeneralPostingGroupBasedOnRegion(acc.Region__c, acc);
				}

				if (acc.Customer_Posting__c == triggerOldMap.get(acc.Id).Customer_Posting__c) {
					changeCustomerPostingGroupBasedOnRegion(acc.Region__c, acc);
				}

				if (acc.VAT__c == triggerOldMap.get(acc.Id).VAT__c) {
					changeVATPostingGroupBasedOnRegion(acc.Region__c, acc);
				}
			}		
		}
	}

	@TestVisible
	private static void changeGeneralPostingGroupBasedOnRegion(final String region, Account acc) {
		switch on region {
			when null {
				acc.General_Picklist__c = 'O_TUZ';
			} when 'SK' {
				acc.General_Picklist__c = 'O_TUZ';
			} when 'EU' {
				acc.General_Picklist__c = 'O_ZAH';
			} when 'non_EU' {
				acc.General_Picklist__c = 'O_ZAH';
			} when else {
				acc.addError('Value of the region parameter has unexpected value.');
			}
		}
	}

	@TestVisible
	private static void changeCustomerPostingGroupBasedOnRegion(final String region, Account acc) {
		switch on region {
			when null {
				acc.Customer_Posting__c = 'O_TUZ';
			} when 'SK' {
				acc.Customer_Posting__c = 'O_TUZ';
			} when 'EU' {
				acc.Customer_Posting__c = 'O_ZAH';
			} when 'non_EU' {
				acc.Customer_Posting__c = 'O_ZAH';
			} when else {
				acc.addError('Value of the region parameter has unexpected value.');
			}
		}
	}

	@TestVisible
	private static void changeVATPostingGroupBasedOnRegion(final String region, Account acc) {
		switch on region {
			when null {
				acc.VAT__c = 'TUZ';
			} when 'SK' {
				acc.VAT__c = 'TUZ';
			} when 'EU' {
				acc.VAT__c = 'ZAH_EUREG';
			} when 'non_EU' {
				acc.VAT__c = 'ZAH_NIE_EU';
			} when else {
				acc.addError('Value of the region parameter has unexpected value.');
			}
		}
	}
}