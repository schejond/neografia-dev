/**
 * @author  Ondrej Schejbal
 * @date    2022-02-17
 */
@IsTest(SeeAllData=false)
private class AccountTriggerHandlerTest {
    @IsTest
    private static void testChangeGeneralPostingGroupBasedOnRegion() {
        Account acc_sk = new Account(Region__c = 'SK');
        Account acc_eu = new Account(Region__c = 'EU');
        Account acc_non_eu = new Account(Region__c = 'non_EU');

        Test.startTest();
        AccountTriggerHandler.changeGeneralPostingGroupBasedOnRegion(acc_sk.Region__c, acc_sk);
        AccountTriggerHandler.changeGeneralPostingGroupBasedOnRegion(acc_eu.Region__c, acc_eu);
        AccountTriggerHandler.changeGeneralPostingGroupBasedOnRegion(acc_non_eu.Region__c, acc_non_eu);
        Test.stopTest();

        System.assertEquals('O_TUZ', acc_sk.General_Picklist__c);
        System.assertEquals('O_ZAH', acc_eu.General_Picklist__c);
        System.assertEquals('O_ZAH', acc_non_eu.General_Picklist__c);
    }

    @IsTest
    private static void testChangeGeneralPostingGroupBasedOnRegion_badRegion() {
        Account acc_bad = new Account();

        Test.startTest();
        try {
            AccountTriggerHandler.changeGeneralPostingGroupBasedOnRegion('xxx', acc_bad);
        } catch (Exception e) {
            System.assert(e.getMessage().contains('Value of the region parameter has unexpected value.'));
        }
        Test.stopTest();
    }

    @IsTest
    private static void testChangeCustomerPostingGroupBasedOnRegion() {
        Account acc_sk = new Account(Region__c = 'SK');
        Account acc_eu = new Account(Region__c = 'EU');
        Account acc_non_eu = new Account(Region__c = 'non_EU');

        Test.startTest();
        AccountTriggerHandler.changeCustomerPostingGroupBasedOnRegion(acc_sk.Region__c, acc_sk);
        AccountTriggerHandler.changeCustomerPostingGroupBasedOnRegion(acc_eu.Region__c, acc_eu);
        AccountTriggerHandler.changeCustomerPostingGroupBasedOnRegion(acc_non_eu.Region__c, acc_non_eu);
        Test.stopTest();

        System.assertEquals('O_TUZ', acc_sk.Customer_Posting__c);
        System.assertEquals('O_ZAH', acc_eu.Customer_Posting__c);
        System.assertEquals('O_ZAH', acc_non_eu.Customer_Posting__c);
    }

    @IsTest
    private static void testChangeCustomerPostingGroupBasedOnRegion_badRegion() {
        Account acc_bad = new Account();

        Test.startTest();
        try {
            AccountTriggerHandler.changeCustomerPostingGroupBasedOnRegion('xxx', acc_bad);
        } catch (Exception e) {
            System.assert(e.getMessage().contains('Value of the region parameter has unexpected value.'));
        }
        Test.stopTest();
    }

    @IsTest
    private static void testChangeVATPostingGroupBasedOnRegion() {
        Account acc_sk = new Account(Region__c = 'SK');
        Account acc_eu = new Account(Region__c = 'EU');
        Account acc_non_eu = new Account(Region__c = 'non_EU');

        Test.startTest();
        AccountTriggerHandler.changeVATPostingGroupBasedOnRegion(acc_sk.Region__c, acc_sk);
        AccountTriggerHandler.changeVATPostingGroupBasedOnRegion(acc_eu.Region__c, acc_eu);
        AccountTriggerHandler.changeVATPostingGroupBasedOnRegion(acc_non_eu.Region__c, acc_non_eu);
        Test.stopTest();

        System.assertEquals('TUZ', acc_sk.VAT__c);
        System.assertEquals('ZAH_EUREG', acc_eu.VAT__c);
        System.assertEquals('ZAH_NIE_EU', acc_non_eu.VAT__c);
    }

    @IsTest
    private static void testChangeVATPostingGroupBasedOnRegion_badRegion() {
        Account acc_bad = new Account();

        Test.startTest();
        try {
            AccountTriggerHandler.changeVATPostingGroupBasedOnRegion('xxx', acc_bad);
        } catch (Exception e) {
            System.assert(e.getMessage().contains('Value of the region parameter has unexpected value.'));
        }
        Test.stopTest();
    }
}