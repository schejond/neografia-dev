/**
 * @author  Ondrej Schejbal
 * @date    2023-04-28
 */
@IsTest
private class SendToNavisionControllerTest
{
    @TestSetup
    private static void prepare()
    {
        Neografia_SOAP_Auth_Settings__c settings = Neografia_SOAP_Auth_Settings__c.getOrgDefaults();
        settings.Username__c= 'user';
        settings.Password__c = 'pswd';
        settings.Domain__c = 'domain';
        settings.IsPreAuthenticate__c = false;
        upsert settings;
    }

    @IsTest
    private static void test_sendAccountToNavision_OKResponse()
    {
        Account parentAcc = new Account(Name = 'Parent Acccount', Customer_Code__c = '66');
        insert parentAcc;
        Account acc = new Account(Name = 'Test Acccount', Customer_Code__c = '666', ParentId = parentAcc.Id);
        insert acc;

        String mockResponse = '<Soap:Envelope xmlns:Soap="http://schemas.xmlsoap.org/soap/envelope/"><Soap:Body><ImportCustomer_Result xmlns="urn:microsoft-dynamics-schemas/codeunit/ImportCustomer"><return_value>OK</return_value></ImportCustomer_Result></Soap:Body></Soap:Envelope>';
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Proxy_Mock(mockResponse, 200, false));

        SendToNavisionController.ServerResponse result = SendToNavisionController.sendAccountToNavision(acc.Id);
        Test.stopTest();

        System.assertEquals(200, result.responseCode);
        System.assertEquals('OK', result.response);
        System.assertEquals('', result.faultCode);
    }

    @IsTest
    private static void test_sendAccountToNavision_errorResponse()
    {
        Account parentAcc = new Account(Name = 'Parent Acccount', Customer_Code__c = '66');
        insert parentAcc;
        Account acc = new Account(Name = 'Test Acccount', Customer_Code__c = '666', ParentId = parentAcc.Id);
        insert acc;

        String mockResponse = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><s:Fault><faultcode xmlns:a="urn:microsoft-dynamics-schemas/error">FaultCode</faultcode><faultstring>Error Message</faultstring></s:Fault></s:Body></s:Envelope>';
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Proxy_Mock(mockResponse, 500, false));

        SendToNavisionController.ServerResponse result = SendToNavisionController.sendAccountToNavision(acc.Id);
        Test.stopTest();

        System.assertEquals(500, result.responseCode);
        System.assertEquals('Error Message', result.response);
        System.assertEquals('FaultCode', result.faultCode);
    }

    @IsTest
    private static void test_sendAccountToNavision_serverException()
    {
        Account parentAcc = new Account(Name = 'Parent Acccount', Customer_Code__c = '66');
        insert parentAcc;
        Account acc = new Account(Name = 'Test Acccount', Customer_Code__c = '666', ParentId = parentAcc.Id);
        insert acc;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Proxy_Mock('', 200, true));
        try
        {
            SendToNavisionController.sendRecordToNavision(acc.Id, 'Account');
        }
        catch (Exception e)
        {
            System.assert(e.getMessage().contains('Script-thrown exception'));
            System.assertEquals('System.AuraHandledException', e.getTypeName());
        }
        Test.stopTest();
    }

    @IsTest
    private static void test_sendAccountToNavision_internalException()
    {
        Account acc = new Account(Name = 'Test Acccount', Customer_Code__c = '666');
        insert acc;
        Contact con = new Contact(LastName = 'Test Contact', AccountId = acc.Id);
        insert con;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Proxy_Mock('', 200, false));
        try
        {
            SendToNavisionController.sendRecordToNavision(con.Id, 'Account');
        }
        catch (Exception e)
        {
            System.assert(e.getMessage().contains('Script-thrown exception'));
            System.assertEquals('System.AuraHandledException', e.getTypeName());
        }
        Test.stopTest();
    }

    @IsTest
    private static void testSendContactToNavision_OKResponse()
    {
        Account acc = new Account(Name = 'Test Acccount', Customer_Code__c = '666');
        insert acc;
        Contact con = new Contact(LastName = 'Test Contact', AccountId = acc.Id);
        insert con;

        String mockResponse = '<Soap:Envelope xmlns:Soap="http://schemas.xmlsoap.org/soap/envelope/"><Soap:Body><ImportContact_Result xmlns="urn:microsoft-dynamics-schemas/codeunit/ImportContact"><return_value>OK</return_value></ImportContact_Result></Soap:Body></Soap:Envelope>';
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Proxy_Mock(mockResponse, 200, false));

        SendToNavisionController.ServerResponse result = SendToNavisionController.sendContactToNavision(con.Id);
        Test.stopTest();

        System.assertEquals(200, result.responseCode);
        System.assertEquals('OK', result.response);
        System.assertEquals('', result.faultCode);
    }

    @IsTest
    private static void testSendContactToNavision_errorResponse()
    {
        Account acc = new Account(Name = 'Test Acccount', Customer_Code__c = '666');
        insert acc;
        Contact con = new Contact(LastName = 'Test Contact', AccountId = acc.Id);
        insert con;

        String mockResponse = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><s:Fault><faultcode xmlns:a="urn:microsoft-dynamics-schemas/error">FaultCode</faultcode><faultstring>Error Message</faultstring></s:Fault></s:Body></s:Envelope>';
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Proxy_Mock(mockResponse, 500, false));

        SendToNavisionController.ServerResponse result = SendToNavisionController.sendContactToNavision(con.Id);
        Test.stopTest();

        System.assertEquals(500, result.responseCode);
        System.assertEquals('Error Message', result.response);
        System.assertEquals('FaultCode', result.faultCode);
    }

    @IsTest
    private static void testSendContactToNavision_serverException()
    {
        Account acc = new Account(Name = 'Test Acccount', Customer_Code__c = '666');
        insert acc;
        Contact con = new Contact(LastName = 'Test Contact', AccountId = acc.Id);
        insert con;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Proxy_Mock('', 200, true));

        try
        {
            SendToNavisionController.sendContactToNavision(con.Id);
        }
        catch (Exception e)
        {
            System.assert(e.getMessage().contains('Script-thrown exception'));
            System.assertEquals('System.AuraHandledException', e.getTypeName());
        }
        Test.stopTest();
    }

    @IsTest
    private static void testSendContactToNavision_internalException()
    {
        Account acc = new Account(Name = 'Test Acccount', Customer_Code__c = '666');
        insert acc;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Proxy_Mock('', 200, false));
        try
        {
            SendToNavisionController.sendRecordToNavision(acc.Id, 'Contact');
        }
        catch (Exception e)
        {
            System.assert(e.getMessage().contains('Script-thrown exception'));
            System.assertEquals('System.AuraHandledException', e.getTypeName());
        }
        Test.stopTest();
    }

    @IsTest
    private static void testUndefinedObjectException()
    {
        Account acc = new Account(Name = 'Test Acccount', Customer_Code__c = '666');
        insert acc;

        Test.startTest();
        try
        {
            SendToNavisionController.sendRecordToNavision(acc.Id, 'xxx');
        }
        catch (Exception e)
        {
            System.assert(e.getMessage().contains('Script-thrown exception'));
            System.assertEquals('System.AuraHandledException', e.getTypeName());
        }
        Test.stopTest();
    }
}